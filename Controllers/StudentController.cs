﻿using Microsoft.AspNetCore.Mvc;
using StudentManagementApi.Models;
using Microsoft.AspNetCore.Http;
using StudentManagementApi.Repository;

namespace StudentManagementApi.Controllers
{
    [Route("api/student")]
    [ApiController]
    public class StudentController : ControllerBase
    {
        /*public List<Student> Students = new List<Student>() {
         new Student() { Id = 1, FirstName = "John",  LastName = "Doe",   Age = 30 , Password="Jhondoe@123",        ConfirmPassword="Jhondoe@123",        Major=" Computer Science",Title="Mr",  IsDeleted=false,Program="Dancing",              Events="Career Fairs",                   Email="Jhondoe13@gmail.com"},
         new Student (){ Id = 2, FirstName = "Jane",  LastName = "Doe",   Age = 25 , Password="Jane@doe1233",       ConfirmPassword="Jane@doe1233",       Major="Biology",          Title="Mrs", IsDeleted=false,Program="Sports",               Events="Club Meetings",                  Email = "Janedoe123@gmail.com"},
         new Student (){ Id = 3, FirstName = "Bob",   LastName = "Smith", Age = 40,  Password="Bobs@42",            ConfirmPassword="Bobs@42",            Major="Commerce",         Title="Mr",  IsDeleted=false,Program="Drawing",              Events="College Tours" ,                 Email="Bobs42@gmail.com"},
         new Student (){ Id = 4, FirstName = "kamal", LastName = "Swain", Age = 22,  Password="Kamal12@swain",      ConfirmPassword="Kamal12@swain",      Major="Botanial",         Title="Mr",  IsDeleted=false,Program="Swimming",             Events = "Commencement Ceremonies",      Email = "Kamal12swain@gmail.com"},
         new Student (){ Id = 5, FirstName = "prince",LastName = "ray",   Age = 21 , Password="Princeray111@3",     ConfirmPassword="Princeray111@3",     Major="Agricultural",     Title="Mr",  IsDeleted=false,Program="Music",                Events = "Concerts",                     Email = "Princeray1113@gmail.com"},
         new Student (){ Id = 6, FirstName = "sajid", LastName = "ali",   Age = 21 , Password="Said@11ali",         ConfirmPassword="Said@11ali",         Major="Criminal Justice", Title="Mr",  IsDeleted=false,Program="Mathematics",          Events = "Conferences",                  Email = "Said11ali@gmail.com"},
         new Student (){ Id = 7, FirstName = "aditya",LastName = "raj",   Age = 21,  Password="Rajaditya@1",        ConfirmPassword="Rajaditya@1",        Major="Economics",        Title="Mr",  IsDeleted=false,Program="Health Sciences" ,     Events = "Cultural Festivals",           Email = "Rajaditya1@gmail.com"},
         new Student (){ Id = 8, FirstName = "harsha",LastName = "cp",    Age = 22 , Password="Hasha111@",          ConfirmPassword="Hasha111@",          Major="Networking",       Title="Mr",  IsDeleted=false,Program="Environmental Science",Events = "Guest Speakers",               Email = "Hasha111@gmail.com"},
         new Student (){ Id = 9, FirstName = "megha", LastName = "kumari",Age = 21 , Password="Meghakumar@111",     ConfirmPassword="Meghakumar@111",     Major="Business",         Title="Mrs", IsDeleted=false,Program="Nursing",              Events = "Health and Wellness Programs", Email = "Meghakumar111@gmail.com"},
         new Student (){ Id = 10,FirstName = "deepti",LastName = "masant",Age = 21 , Password="Deepti1528@masant",  ConfirmPassword="Deepti1528@masant",  Major="Accountant",       Title="Mrs", IsDeleted=false,Program="Journalism",           Events = "Leadership Workshops",         Email = "Deepti1528masant@gmail.com"}
         };*/

        public List<Student> Students = new List<Student>();
        private readonly IStudentRepository _studentRepository;

        public StudentController(IStudentRepository studentRepository)
        {
            _studentRepository = studentRepository;
        }

        [HttpGet]
        public async Task<IActionResult> GetAll()
        {
            var students = await _studentRepository.GetStudents();
            return Ok(students);
        }

        [HttpGet]
        [Route("{Id}")]
        public async Task<IActionResult> Get(int Id)
        {
            var students = await _studentRepository.GetStudents(Id);
            if (students == null)
            {
                return NotFound();
            }
            return Ok(students);
        }

    
    [HttpPost]
        public async Task<IActionResult> Add(Student student)
        {
            // ADD INTO USERS LIST
            var students = await _studentRepository.RegisterStudents(student);
            return Ok(student);
        }

        [HttpPut]
        [Route("{Id}")]
        public async Task<IActionResult> UpdateStudent( [FromBody] Student Students,int Id /*int GetStudents*/)
        {
            if (Students.Id == 0)
            {
                return BadRequest("Id parameter cannot be 0");
            }

            if (Id != Students.Id)
            {
                return BadRequest("Provided ID does not match with the ID in the request body");
            }

            var existingStudent = await _studentRepository.GetStudentById(Id);
            if (existingStudent == null)
            {
                return NotFound($"Student with ID {Id} not found");
            }

            var updatedStudent = await _studentRepository.UpdateStudents(Students);
            return Ok(updatedStudent);



            /* var existingStudent = await _studentRepository.UpdateStudents(Students);
              if (existingStudent == null)
         {
              return NotFound($"Student with ID {Students.Id} not found");
         }*/
            /* var students = await _studentRepository.UpdateStudents(Students);
             if (students.Id != GetStudents)
         {
             return NotFound($"Student with ID {GetStudents} not found ");
         }*/
            // REMOVE USER FOR UPDATE
            // Students.Remove(students);

            // CREATE A NEW USER
            Student student = new Student()
            {
                Id = Students.Id,
                FirstName = Students.FirstName,
                Email = Students.Email,
                LastName = Students.LastName,
                Age = Students.Age,
                Password = Students.Password,
                ConfirmPassword=Students.ConfirmPassword,   
                Major= Students.Major
            };

            // ADD INTO USERS LIST
           // Students.Add(student);

            return Ok(Students);
        }
        /*[HttpDelete]
        [Route("{Id}")]
            public async Task<IActionResult> DeleteStudent(int Id)
            {
                var students = await _studentRepository.DeleteStudents(Id);
                if (students == null)
            {
                return NotFound("null parameter");
            }
            //Students.Remove(students);
            return Ok(students);
        }*/

        /*[HttpDelete]
        public async Task<IActionResult> DeleteAll()
        {
            var students = await _studentRepository.DeleteStudents();
            return Ok(students);
        }*/

        [HttpDelete]
        [Route("{Id}")]
        public async Task<IActionResult> DeleteStudent(int Id)
        {

            var isDeleted = await _studentRepository.DeleteStudents(Id);

            if (!isDeleted)
            {
                return NotFound();
            }

            return Ok(new { status = true, message = "Deleted Successfully." });
        }

    }
}
