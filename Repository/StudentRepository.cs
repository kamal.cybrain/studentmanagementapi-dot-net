﻿using System.Data;
using System.Data.SqlClient;
using System.Security.Cryptography.X509Certificates;
using StudentManagementApi.Models;


namespace StudentManagementApi.Repository
{
    public interface IStudentRepository
    {
        Task<List<Student>> GetStudents();
        Task<Student> GetStudents(int Id);
        Task<Student> RegisterStudents(Student Students);
        Task<Student> UpdateStudents(Student Students );
        Task<bool> DeleteStudents(int Id);
        Task<Student> GetStudentById(int Id);
    }
    public class StudentRepository  : IStudentRepository
    {
        private readonly IConfiguration _config;

        public StudentRepository(IConfiguration configuration)
        {
            _config = configuration;
        }

        public async Task<Student?> GetStudents(int Id)
        {
            Student Students = null;

            using (var connection = new SqlConnection(_config.GetConnectionString("DefaultConnection")))
            {
                connection.Open();

                string sqlText = @"SELECT * FROM StudentTables WHERE Id = " + Id.ToString();

                SqlCommand command = new SqlCommand(sqlText, connection)
                {
                    CommandType = CommandType.Text
                };

                using (var reader = await command.ExecuteReaderAsync())
                {
                    while (await reader.ReadAsync())
                    {
                        Students = new Student()
                        {
                            Id = Convert.ToInt32(reader["Id"]),
                            FirstName = Convert.ToString(reader["FirstName"]),
                            Email = Convert.ToString(reader["Email"]),
                            LastName = Convert.ToString(reader["LastName"]),
                            Password = Convert.ToString(reader["Password"]),
                            ConfirmPassword = Convert.ToString(reader["ConfirmPassword"]),
                            Title = Convert.ToString(reader["Title"]),
                            Event= Convert.ToString(reader["Event"]),
                            Program = Convert.ToString(reader["Program"]),
                            Age = Convert.ToInt32(reader["Age"]),
                            Major = Convert.ToString(reader["Major"]),
                            IsDeleted = Convert.ToBoolean(reader["IsDeleted"]),
                        };
                    }
                }
            }

            return Students;
        }


        public async Task<List<Student>> GetStudents()
        {
            List<Student> students = new List<Student>();

            using (var connection = new SqlConnection(_config.GetConnectionString("DefaultConnection")))
            {
                connection.Open();

                SqlCommand command = new SqlCommand("StudentGetData", connection)
                {
                    CommandType = CommandType.StoredProcedure
                };

                using (var reader = await command.ExecuteReaderAsync())
                {
                    while (await reader.ReadAsync())
                    {
                        students.Add(new Student()
                        {
                            Id = Convert.ToInt32(reader["Id"]),
                            FirstName = Convert.ToString(reader["FirstName"]),
                            Email = Convert.ToString(reader["Email"]),
                            LastName = Convert.ToString(reader["LastName"]),
                            Password = Convert.ToString(reader["Password"]),
                            ConfirmPassword = Convert.ToString(reader["ConfirmPassword"]),
                            Title = Convert.ToString(reader["Title"]),
                            Event = Convert.ToString(reader["Event"]),
                            Program = Convert.ToString(reader["Program"]),
                            Age = Convert.ToInt32(reader["Age"]),
                            Major = Convert.ToString(reader["Major"]),
                            IsDeleted = Convert.ToBoolean(reader["IsDeleted"]),
                        });
                    }
                }
            }

            return students;
        }

        public async Task<Student?> RegisterStudents(Student Students)
        {
            Student students = Students ;

            using (var connection = new SqlConnection(_config.GetConnectionString("DefaultConnection")))
            {
                connection.Open();

                SqlParameter[] parameters = new SqlParameter[7];
                parameters[0] = new SqlParameter("@FirstName", SqlDbType.VarChar, 250);
                parameters[0].Value = Students.FirstName;

                parameters[1] = new SqlParameter("@Email", SqlDbType.VarChar, 200);
                parameters[1].Value = Students.Email;

                parameters[2] = new SqlParameter("@LastName", SqlDbType.VarChar, 250);
                parameters[2].Value = Students.LastName;

                parameters[3] = new SqlParameter("@Major", SqlDbType.VarChar,100);
                parameters[3].Value = Students.Major;

                parameters[4] = new SqlParameter("@Age", SqlDbType.Int);
                parameters[4].Value = Students.Age;

                parameters[5] = new SqlParameter("@Password", SqlDbType.VarChar,200);
                parameters[5].Value = Students.Password;

                parameters[6] = new SqlParameter("@ConfirmPassword", SqlDbType.VarChar,200);
                parameters[6].Value = Students.ConfirmPassword;

                

                SqlCommand command = new SqlCommand("StudentNewData", connection)
                {
                    CommandType = CommandType.StoredProcedure,
                };

                foreach (SqlParameter param in parameters)
                {
                    command.Parameters.Add(param);
                }

                using (var reader = await command.ExecuteReaderAsync())
                {
                    while (await reader.ReadAsync())
                    {
                        students = new Student()
                        {
                            Id = Convert.ToInt32(reader["Id"]),
                            FirstName = Convert.ToString(reader["FirstName"]),
                            Email = Convert.ToString(reader["Email"]),
                            LastName = Convert.ToString(reader["LastName"]),
                            Password = Convert.ToString(reader["Password"]),
                            ConfirmPassword = Convert.ToString(reader["ConfirmPassword"]),
                            Title = Convert.ToString(reader["Title"]),
                            Event = Convert.ToString(reader["Event"]),
                            Program = Convert.ToString(reader["Program"]),
                            Age = Convert.ToInt32(reader["Age"]),
                            Major = Convert.ToString(reader["Major"]),
                            IsDeleted = Convert.ToBoolean(reader["IsDeleted"]),
                        };
                    }
                }
            }

            return students;
        }
        public async Task<Student?> UpdateStudents(Student Students)
        {

            Student students = Students;

            using (var connection = new SqlConnection(_config.GetConnectionString("DefaultConnection")))
            {
                connection.Open();

                SqlParameter[] parameters = new SqlParameter[8];
                parameters[0] = new SqlParameter("@Id", SqlDbType.Int);
                parameters[0].Value = Students.Id;


                parameters[1] = new SqlParameter("@FirstName", SqlDbType.VarChar, 250);
                parameters[1].Value = Students.FirstName;

                parameters[2] = new SqlParameter("@Email", SqlDbType.VarChar, 200);
                parameters[2].Value = Students.Email;

                parameters[3] = new SqlParameter("@LastName", SqlDbType.VarChar, 250);
                parameters[3].Value = Students.LastName;

                parameters[4] = new SqlParameter("@Major", SqlDbType.VarChar, 100);
                parameters[4].Value = Students.Major;

                parameters[5] = new SqlParameter("@Age", SqlDbType.Int);
                parameters[5].Value = Students.Age;

                parameters[6] = new SqlParameter("@Password", SqlDbType.VarChar, 200);
                parameters[6].Value = Students.Password;

                parameters[7] = new SqlParameter("@ConfirmPassword", SqlDbType.VarChar, 200);
                parameters[7].Value = Students.ConfirmPassword;



                SqlCommand command = new SqlCommand("UpdateStudentData", connection)
                {
                    CommandType = CommandType.StoredProcedure,
                };

                foreach (SqlParameter param in parameters)
                {
                    command.Parameters.Add(param);
                }

                using (var reader = await command.ExecuteReaderAsync())
                {
                    while (await reader.ReadAsync())
                    {
                        students = new Student()
                        {
                            Id = Convert.ToInt32(reader["Id"]),
                            FirstName = Convert.ToString(reader["FirstName"]),
                            Email = Convert.ToString(reader["Email"]),
                            LastName = Convert.ToString(reader["LastName"]),
                            Password = Convert.ToString(reader["Password"]),
                            ConfirmPassword = Convert.ToString(reader["ConfirmPassword"]),
                            Title = Convert.ToString(reader["Title"]),
                            Event = Convert.ToString(reader["Event"]),
                            Program = Convert.ToString(reader["Program"]),
                            Age = Convert.ToInt32(reader["Age"]),
                            Major = Convert.ToString(reader["Major"]),
                            IsDeleted = Convert.ToBoolean(reader["IsDeleted"]),
                        };
                    }
                }
            }

            return students;
        }

        /*public async Task<Student?> DeleteStudents(int Id)
        {
            Student students = Students;

            using (var connection = new SqlConnection(_config.GetConnectionString("DefaultConnection")))
            {
                connection.Open();

                SqlParameter[] parameters = new SqlParameter[1];
                parameters[0] = new SqlParameter("@Id", SqlDbType.Int);
                parameters[0].Value = Id;

                SqlCommand command = new SqlCommand("StudentDeleteData", connection)
                {
                    CommandType = CommandType.StoredProcedure,
                };
                foreach (SqlParameter param in parameters)
                {
                    command.Parameters.Add(param);
                }


                using (var reader = await command.ExecuteReaderAsync())
                {
                    while (await reader.ReadAsync())
                    {
                        Students = new Student()
                        {
                            Id = Convert.ToInt32(reader["Id"]),
                            FirstName = Convert.ToString(reader["FirstName"]),
                            Email = Convert.ToString(reader["Email"]),
                            LastName = Convert.ToString(reader["LastName"]),
                            Password = Convert.ToString(reader["Password"]),
                            ConfirmPassword = Convert.ToString(reader["ConfirmPassword"]),
                            Title = Convert.ToString(reader["Title"]),
                            Event = Convert.ToString(reader["Event"]),
                            Program = Convert.ToString(reader["Program"]),
                            Age = Convert.ToInt32(reader["Age"]),
                            Major = Convert.ToString(reader["Major"]),
                            IsDeleted = Convert.ToBoolean(reader["IsDeleted"]),
                        };
                    }
                }
            }

            return Students;
        }*/

        /*public async Task<List<Student?>> DeleteStudents()
        {
            List<Student> students = new List<Student>();

            using (var connection = new SqlConnection(_config.GetConnectionString("DefaultConnection")))
            {
                connection.Open();

                SqlCommand command = new SqlCommand("DeleteStudentData", connection)
                {
                    CommandType = CommandType.StoredProcedure
                };

                using (var reader = await command.ExecuteReaderAsync())
                {
                    while (await reader.ReadAsync())
                    {
                        students.Add(new Student()
                        {
                            Id = Convert.ToInt32(reader["Id"]),
                            FirstName = Convert.ToString(reader["FirstName"]),
                            Email = Convert.ToString(reader["Email"]),
                            LastName = Convert.ToString(reader["LastName"]),
                            Password = Convert.ToString(reader["Password"]),
                            ConfirmPassword = Convert.ToString(reader["ConfirmPassword"]),
                            Title = Convert.ToString(reader["Title"]),
                            Event = Convert.ToString(reader["Event"]),
                            Program = Convert.ToString(reader["Program"]),
                            Age = Convert.ToInt32(reader["Age"]),
                            Major = Convert.ToString(reader["Major"]),
                            IsDeleted = Convert.ToBoolean(reader["IsDeleted"]),
                        });
                    }
                }
            }

            return students;
        }*/

        public async Task<bool> DeleteStudents(int Id)
        {

            Student Students = null;

            using (var connection = new SqlConnection(_config.GetConnectionString("DefaultConnection")))
            {
                connection.Open();

                string sqlText = @"UPDATE StudentTables SET IsDeleted = 1 WHERE Id = " + Id.ToString();

                SqlCommand command = new SqlCommand(sqlText, connection)
                {
                    CommandType = CommandType.Text
                };

                int result = await command.ExecuteNonQueryAsync();

                return result > 0;
            }

           

            /*public async Task<bool> DeleteStudents(int Id)
            {
                using (var connection = new SqlConnection(_config.GetConnectionString("DefaultConnection")))
                {
                    connection.Open();

                    string sqlText = @"IF NOT EXISTS (SELECT * FROM sys.columns WHERE object_id = OBJECT_ID('StudentTables') AND name = 'IsDelete')
                                ALTER TABLE StudentTables ADD IsDelete BIT NOT NULL DEFAULT 0;

                                UPDATE StudentTables SET IsDelete = 1 WHERE Id = @Id";

                    SqlCommand command = new SqlCommand(sqlText, connection);
                    command.CommandType = CommandType.Text;
                    command.Parameters.AddWithValue("@Id", Id);

                    int result = await command.ExecuteNonQueryAsync();

                    return result > 0;
                }*/



        }

        public async Task<Student?> GetStudentById(int Id)
        {
            using (var connection = new SqlConnection(_config.GetConnectionString("DefaultConnection")))
            {
                connection.Open();

                 string sqlText = @"SELECT * FROM StudentTables WHERE Id = " + Id.ToString();

                SqlCommand command = new SqlCommand(sqlText, connection);
                {
                    command.CommandType = CommandType.Text;
                };

                using (var reader = await command.ExecuteReaderAsync())
                {
                    while (await reader.ReadAsync())
                    {
                        return new Student()
                        {
                            Id = Convert.ToInt32(reader["Id"]),
                            FirstName = Convert.ToString(reader["FirstName"]),
                            Email = Convert.ToString(reader["Email"]),
                            LastName = Convert.ToString(reader["LastName"]),
                            Password = Convert.ToString(reader["Password"]),
                            ConfirmPassword = Convert.ToString(reader["ConfirmPassword"]),
                            Title = Convert.ToString(reader["Title"]),
                            Event = Convert.ToString(reader["Event"]),
                            Program = Convert.ToString(reader["Program"]),
                            Age = Convert.ToInt32(reader["Age"]),
                            Major = Convert.ToString(reader["Major"]),
                            IsDeleted = Convert.ToBoolean(reader["IsDeleted"]),
                        };
                    }
                }
            }

            return null;
        }
    }
}


