﻿using System.Runtime.CompilerServices;
using System.ComponentModel.DataAnnotations;

namespace StudentManagementApi.Models
{
    public class Student
    {

        public int Id { get; set; }
        [Required(ErrorMessage = "Please provide a first name.")]
        [StringLength(50, MinimumLength = 2, ErrorMessage = "First name must be between 2 and 50 characters.")]
        public string FirstName { get; set; }
        [Required(ErrorMessage = "Please provide a last name.")]
        [StringLength(50, MinimumLength = 2, ErrorMessage = "last name must be between 2 and 50 characters.")]
        public string LastName { get; set; }
        [Required(ErrorMessage = "Please provide an email address.")]
        [EmailAddress(ErrorMessage = "Please provide a valid email address.")]
        public string Email { get; set; }

        [Required(ErrorMessage = "Please provide a password.")]
        [RegularExpression(@"^(?=.*[a-z])(?=.*[A-Z])(?=.*\d)(?=.*[^\da-zA-Z]).{8,}$",
            ErrorMessage = "Password must be at least 8 characters long, and contain at least one uppercase letter, one lowercase letter, one number, and one special character.")]
        public string Password { get; set; }

        [Required(ErrorMessage = "Please confirm your password.")]
        [Compare("Password", ErrorMessage = "Password and confirmation password do not match.")]
        public string ConfirmPassword { get; set; }

        [Required(ErrorMessage = "Please provide a major.")]
        [StringLength(20, MinimumLength = 2, ErrorMessage = "Major must be between 2 and 20 characters.")]
        public string Major { get; set; }

        [Required(ErrorMessage = "Please provide your Age.")]
        [Range(18, 45, ErrorMessage = "Age must be between 18 and 45.")]
        public int Age { get; set; }
        public bool IsDeleted { get; set; }
        public string Title { get; set; }

        public string Program { get; set; }
        public string Event { get; set; }

        internal static void Add(Student user)
        {
            throw new NotImplementedException();
        }
    }
}