using Microsoft.OpenApi.Models;
using StudentManagementApi.Repository;


var builder = WebApplication.CreateBuilder(args);

// Add services to the container.

builder.Services.AddControllers();
// Learn more about configuring Swagger/OpenAPI at https://aka.ms/aspnetcore/swashbuckle
builder.Services.AddEndpointsApiExplorer();
builder.Services.AddSwaggerGen(config =>
{
    config.SwaggerDoc("v1", new OpenApiInfo()
    {
        Version = "v1",
        Title = "Student Management Api",
        Description = "Student Api",
    });
});

builder.Services.AddCors(config =>
{
    config.AddPolicy("AllowedOrigin", option =>
    {
        option.SetIsOriginAllowed(origin => true);
        option.AllowAnyHeader();
        option.AllowAnyMethod();
        option.AllowCredentials();
    });
});

// ADD REPOSITORIES
builder.Services.AddScoped<IStudentRepository, StudentRepository>();

var app = builder.Build();

// Configure the HTTP request pipeline.
if (app.Environment.IsDevelopment())
{
    app.UseSwagger();
    app.UseSwaggerUI();
}

app.UseHttpsRedirection();

app.UseAuthorization();

app.MapControllers();

app.Run();
